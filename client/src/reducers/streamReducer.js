import _ from 'lodash';
import * as ACTION from '../actions/types';
import { handleActions } from 'redux-actions';

const streamReducer = handleActions(
  {
    [ACTION.FETCH_STREAMS]: (state, action) => ({
      ...state,
      ..._.mapKeys(action.payload, 'id')
    }),
    [ACTION.FETCH_STREAM]: (state, action) => ({
      ...state,
      [action.payload.id]: action.payload
    }),
    [ACTION.CREATE_STREAM]: (state, action) => ({
      ...state,
      [action.payload.id]: action.payload
    }),
    [ACTION.EDIT_STREAM]: (state, action) => ({
      ...state,
      [action.payload.id]: action.payload
    }),
    [ACTION.DELETE_STREAM]: (state, action) => {
      return _.omit(state, action.payload)
    }
  },
  {}
);

export default streamReducer;