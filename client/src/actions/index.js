import API from '../api/streams';
import history from '../history';
import * as ACTION from './types';

export const signIn = userId => ({ 
  type: ACTION.SIGN_IN,
  payload: userId
});

export const signOut = () => ({ type: ACTION.SIGN_OUT });

export const createStream = formValues => async (dispatch, getState) => {
  const { userId } = getState().auth;
  const response = await API.post('/streams', { ...formValues, userId });

  dispatch({ type: ACTION.CREATE_STREAM, payload: response.data });
  history.push('/');
};

export const fetchStreams = () => async dispatch => {
  const response = await API.get('/streams');

  dispatch({ type: ACTION.FETCH_STREAMS, payload: response.data });
};

export const fetchStream = (id) => async dispatch => {
  const response = await API.get(`/streams/${id}`);

  dispatch({ type: ACTION.FETCH_STREAM, payload: response.data });
};

export const editStream = (id, formValues) => async (dispatch, getState) => {
  const { userId } = getState().auth;
  const response = await API.put(`/streams/${id}`, {...formValues, userId});

  dispatch({ type: ACTION.EDIT_STREAM, payload: response.data });
  history.push('/');
};

export const deleteStream = (id) => async dispatch => {
  await API.delete(`/streams/${id}`);

  dispatch({ type: ACTION.DELETE_STREAM, payload: id });
  history.push('/');
};