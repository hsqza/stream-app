import _ from 'lodash';
import React from 'react';
import { fetchStream, editStream } from '../../actions';
import StreamForm from './StreamForm';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

class StreamEdit extends React.Component {
  static propTypes = {
    fetchStream: PropTypes.func.isRequired,
    stream: PropTypes.object,
    match: PropTypes.shape({
      id: PropTypes.number
    })
  }

  componentDidMount() {
    const { fetchStream, match } = this.props;
    fetchStream(match.params.id);
  }

  onSubmit = (formValues) => {
    const { editStream, match } = this.props;

    editStream(match.params.id, formValues);
  }

  render () {
    const { stream } = this.props;

    if (!stream) {
      return <div>Loading ...</div>;
    }
    return (
      <div>
        <h3>Edit a stream</h3>
        <StreamForm
          onSubmit={this.onSubmit}
          initialValues={_.pick(stream, 'title', 'description')}/>
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => ({
  stream: state.streams[ownProps.match.params.id]
});

export default connect(mapStateToProps, {fetchStream, editStream})(StreamEdit);