import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { fetchStreams } from '../../actions/';

class StreamList extends React.Component {

  componentDidMount() {
    const { fetchStreams } = this.props;
    fetchStreams();
  }

  renderAdmin = (admin) => {
    const { currentUserId } = this.props;

    if (admin.userId === currentUserId) {
      return (
        <div className="right floated content">
          <Link to={`/streams/edit/${admin.id}`} className="ui button primary">Edit</Link>
          <Link to={`/streams/delete/${admin.id}`} className="ui button negative">Delete</Link>
        </div>
      );
    }
  }

  singleItemList = () => {
    const { lists } = this.props;

    return lists.map( list => (
      <div className="item" key={list.id}>
        {this.renderAdmin(list)}
        <i className="large middle aligned icon camera" />
        <div className="content">
          <Link to={`streams/${list.id}`} className="header">{list.title}</Link>
          <div className="description">{list.description}</div>
        </div>
      </div>
    ));
  }

  renderCreate = () => {
    const { isSignedIn } = this.props;

    if (isSignedIn) {
      return (
        <div style={{textAlign: 'right'}}>
          <Link to="/streams/new" className="ui button primary">Create link</Link>
        </div>
      );
    }
  }

  render() {
    return(
      <div>
        <h2>StreamList</h2>
        <div className="ui celled list">{this.singleItemList()}</div>
        {this.renderCreate()}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  lists: Object.values(state.streams),
  currentUserId: state.auth.userId,
  isSignedIn: state.auth.isSignedIn
});

export default connect(mapStateToProps, { fetchStreams })(StreamList);