import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';
import auth from './authReducer';
import streamReducer from './streamReducer';

export default combineReducers({
  form: formReducer,
  auth,
  streams: streamReducer
});