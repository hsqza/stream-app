import React, { Component, Fragment } from 'react';
import { Link } from 'react-router-dom';
import Modal from '../Modal';
import history from '../../history';
import { connect } from 'react-redux';
import { fetchStream, deleteStream } from '../../actions';
import PropTypes from 'prop-types';

class StreamDelete extends Component {

  static propTypes = {
    fetchStream: PropTypes.func.isRequired,
    deleteStream: PropTypes.func.isRequired,
    match: PropTypes.shape({
      id: PropTypes.number
    })
  }

  componentDidMount() {
    const { fetchStream, match } = this.props;

    fetchStream(match.params.id);
  }


  renderActions = () => {
    const { deleteStream, match } = this.props;
    return (
      <Fragment>
        <Link to='/' className="ui button">Cancel</Link>
        <button onClick={() => deleteStream(match.params.id)} className="ui button negative">Delete</button>
      </Fragment>
    );
  }

  renderContent = stream => {
    if (!stream) {
      return <div>Loading ...</div>;
    }
    return <p>Are you sure you want to delete: <strong>{stream.title} ?</strong></p>;
  }

  render() {
    const { stream } = this.props;

    return (
      <div>
        <Modal
          title='Delete Stream'
          content={this.renderContent(stream)}
          actions={this.renderActions()}
          onDismiss={() => history.push('/')}
        />
      </div>
    );
  }
}

const mapStateToProps = (state, props) => ({
  stream: state.streams[props.match.params.id]
});

export default connect(mapStateToProps, { fetchStream, deleteStream })(StreamDelete);