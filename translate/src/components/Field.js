import React, { Component } from 'react';
import LanguageContext from '../contexts/LanguageContext';

class Field extends Component {
  static contextType = LanguageContext;

  render() {
    const { language } = this.context;

    const text = language === 'english' ? 'Name' : 'Imię';

    return (
      <div className="ui field">
        <label>{text}</label>
        <input type="text" />
      </div>
    )
  }
}

export default Field;
