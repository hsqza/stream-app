import * as ACTION from '../actions/types';
import { handleActions } from 'redux-actions';

const INITIAL_STATE = {
  isSignedIn: null,
  userId: null
};

const auth = handleActions(
  {
    [ACTION.SIGN_IN]: (state, action) => ({
      ...state,
      isSignedIn: true,
      userId: action.payload
    }),
    [ACTION.SIGN_OUT]: (state, action) => ({
      ...state,
      isSignedIn: false,
      userId: null
    })
  },
  INITIAL_STATE
)

export default auth;