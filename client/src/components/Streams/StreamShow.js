import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fetchStream } from '../../actions';
import flv from 'flv.js';
import PropTypes from 'prop-types';

class StreamShow extends Component {
  static propTypes = {
    fetchStream: PropTypes.func.isRequired
  }

  constructor(props) {
    super(props);

    this.videoRef = React.createRef();
  }

  componentDidMount() {
    const { fetchStream, match } = this.props;
    const { id } = match.params;

    fetchStream(id).then(() => {
      this.player = flv.createPlayer({
        type: 'flv',
        url: `http://localhost:8000/live/${id}.flv`
      });
      this.player.attachMediaElement(this.videoRef.current);
      this.player.load();
    });
  }

  componentWillUnmount() {
    this.player.destroy();
  }

  render() {
    const { stream } = this.props;
    if (!stream) return <div>Loading ...</div>;

    const { title, description } = stream;
    return (
      <div>
        <video ref={this.videoRef} style={ {width: '100%'} } controls></video>
        <h1>{title}</h1>
        <h5>{description}</h5>
      </div>
    );
  }
}

const mapStateToProps = (state, props) => ({
  stream: state.streams[props.match.params.id]
});

export default connect(mapStateToProps, { fetchStream })(StreamShow);